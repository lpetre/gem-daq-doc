# Legacy GE1/1 operations

## Connecting to the setups

### b904 setup

Login as `gemuser` into `gem904daq01` account via SSH (`ssh gemuser@gem904daq01`) from inside the CERN GPN (e.g. from `lxplus`).

**Make sure** that the `DATA_PATH` is set to `/data/bigdisk/GEM-Data-Taking/GE11_Integration/` (`export DATA_PATH=/data/bigdisk/GEM-Data-Taking/GE11_Integration/`).

### p5 setup

Login into your `cmsusr` account via SSH `ssh <your_nice_username>@cmsusr.cern.ch` from inside the CERN GPN (e.g. from `lxplus`). Then connect as `gempro` with `sudo -u gempro -i`. This account is configured to allow password-less login on all GEM machines (and more). The machine to be used is `gemvm-legacy` (`ssh gemvm-legacy`).

### A few tips

* The following convention is used throughout this document: `(crate/shelf, slot/AMC, OH): (XX, YY, ZZ)`
* Different `tmux` sessions are available to share your work with others as well as to allow long duration commands to run in case your connection breaks. The `tmux` sessions can be listed with `tmux ls` and one can attach the desired session with `tmux a -t <session_name>`. To detach from `tmux` session, `Ctrl+b` followed by `d` has to be used.
* In case the `tmux` sessions are not opened, the following command can be used to start them: `tmux start-server \; source-file $DATA_PATH/config/sessions.tmux`
* (b904-specific) The chamber info file is located in `/home/gemuser/gemdaq/config/system_specific_constants.py`
* (p5-specific) The chamber info file is located in `$DATA_PATH/config/python/system_specific_constants.py`

## Recovering the back-end

This procedure shouldn't be followed during normal operations and left to the DAQ experts.

### Check that all CTP7 are reachable

Due to a bug in between the MCH and the system manager, some CTP7 could not get their geographical IP addresses. One must make sure that all CTP7 are reachable:

```
for i in {01..11..2}; do ping -c1 -W1 gem-shelfXX-amc$i; done
```

In case one of the CTP7 is not reachable, the corresponding MCH must be rebooted until all CTP7 are reachable:

```
telnet <mch_hostname>
> reboot
```

### Recovering the AMC13

```
AMC13Tool2.exe -c /opt/cmsgemos/etc/maps/connections.xml -i gem.shelfXX.amc13
>en 1,3,5,7,9,11 n
```

### Recovering the CTP7

The legacy software runs as `gemuser` and can be recovered with the following procedure. It ensures that no old `rpcsvc` and `ipbus` daemons are left running and that the back-end is fully functional with the legacy software.

```
mussh -m 12 -H $DATA_PATH/config/ctp7_hosts.txt -l root -c 'bash -c "killall -9 rpcsvc; killall -9 ipbus"'
mussh -m 12 -H $DATA_PATH/config/ctp7_hosts.txt -l gemuser -c 'bash -l -c "recover.sh; rpcsvc; ipbus"'
```

With the newest releases of the backend firmware, the TTC clock phase must be manually aligned for every board in `gem_reg.py`.

```
connect gem-shelfXX-amcYY
write GEM_AMC.TTC.CTRL.LOCKMON_TARGET_PHASE 0x100
write GEM_AMC.TTC.CTRL.PHASE_ALIGNMENT_RESET 1
write GEM_AMC.TTC.CTRL.CNT_RESET 1
kw GEM_AMC.TTC.STATUS.CLK.PHASE_MONITOR.PHASE
```

Then check that it roughly matches the `GEM_AMC.TTC.CTRL.LOCKMON_TARGET_PHASE` value (i.e. `0x100`). If it doesn't, repeat the procedure.

Once all boards have their TTC clock phase correctly aligned, the MGT must be reset.

```
mussh -m 12 -H $DATA_PATH/config/ctp7_hosts.txt -l gemuser -c 'bash -l -c "gth_reset.sh; gth_status.sh"'
```

## Operations

Currently, all calibration operations are run with the legacy software.

### Initial setup

#### RX optical power measurement

After connecting fiber, or in case of doubt, the RX optical power on the back-end board can be measured with the `fiber_test.py` tool.

```
for i in {01..11..2}; do ssh gemuser@gem-shelfXX-amc${i} 'bash -c "for i in {0..11}; do python /mnt/persistent/gemdaq/python/reg_utils/fiber_test.py -oh \${i}; done"'; done
```

#### Establishing the communication

Establishing the communication consists programming the GBTx and taking the GBT phase scan. The trigger links are not tested at that point, so bad trigger links are are accepted. **Note that the default LUT for phases depends on the chamber length. Different `ELOG_PATH` are used not to override the outputs between different script invocations.**

```
ELOG_PATH=$ELOG_PATH/gem-shelfXX-amcYYs ~/scripts/testConnectivity.py --gemType ge11 --detType=short XX YY 0x333 -a --writePhases2File --skipDACScan --skipScurve
```

Short chambers are those in the mask `0x333`, while long chambers are those in the mask `0xccc`

Don't forget to create the temporary `ELOG_PATH` prior to launching the commands with, for example, `for length in s l; do for i in {01..11..2}; do mkdir -p $ELOG_PATH/gem-shelfXX-amc${i}${length}/; done; done`

Once good phases have been found, the results can be saved in the `$DATA_PATH`. Make sure that the files are properly formatted!

```
for i in {01..11..2}; do sort -n -k1 -k2 $ELOG_PATH/gem-shelfXX-amc${i}{s,l}/phases.log > $DATA_PATH/config/gbt/gem-shelfXX-amc${i}-phases.txt; done
```

**Warning:** Using `--acceptBadGBTLink` is necessary if and only if the GBT link do not lock. Use the option with care! Useful checks are disabled! check before using `gem_reg.py` if the GBT are locked properly, in this way:
```
connect gem-shelfXX-amcYY
rwc GEM_AMC.OH_LINKS.OH*GBT*READY
```

The configurations files with the correct GBT phases used for the configuration of the detector are `$DATA_PATH/config/gbt/gem-shelfXX-amcYY-phases.txt`. Update the files with the newest values of the phases, tracking the differences between the previous version and the new one, if needed (relevant changes are if the phase is/was set to 15 or if it is shifted by 3/4 units). If VFAT and GBT communication statuses are different, they should be traked in the [GitLab issues](https://gitlab.cern.ch/cmsgemonline/gem-ops/gem-issue-tracker/-/issues).

#### Taking the DAC scans

Once the communication is established with the chambers, the DAC scans can be taken. Prior to taking any DAC scan, the VFAT configuration files on the CTP7 are reset in order to start from a clean state.

```
mussh -m 12 -H $DATA_PATH/config/ctp7_hosts.txt -l gemuser -c 'bash -l -c "for i in {0..11}; do python /mnt/persistent/gemdaq/scripts/setLinks.py -g \${i}; done"'
for i in {0..11}; do getCalInfoFromDB.py XX YY $i --write2File --write2CTP7; done
~/scripts/testConnectivity.py --gemType ge11 --detType=short XX YY 0xfff -a --skipGBTPhaseScan --acceptBadDACFits --skipScurve -i -f5
```

The output files should be inspected and any result that could indicate a broken DAC circuit should be reported in the [GitLab issues](https://gitlab.cern.ch/cmsgemonline/gem-ops/gem-issue-tracker/-/issues).

### Data-taking procedures

#### Establish communication

Once chambers have already undergone the initial setup, establishing the communication is much easier and faster.

```
~/scripts/testConnectivity.py --gemType ge11 --detType=short XX YY 0xfff -a --skipGBTPhaseScan --skipDACScan --skipScurve -i
writeGBTPhase.py all XX YY $DATA_PATH/config/gbt/gem-shelfXX-amcYY-phases.txt
~/scripts/testConnectivity.py --gemType ge11 --detType=short XX YY 0xfff -a --skipGBTPhaseScan --skipDACScan --skipScurve -i -f5
```

#### Configure the chambers

Chambers can be configured with the following for loop.

```
for i in {0..11}; do confChamber.py --shelf XX --slot YY -g $i --run --zeroChan --detType short --gemType=ge11; done
```

If the configuration fails for some chamber, check whether this is a known issue. In this case, launch the configuration command for that specific chamber with the appropriate VFAT mask.

#### S-curves

After the chambers are configured, taking S-curves is as easy as:

```
run_scans.py --gemType ge11 --detType short scurve XX YY 0xfff
```

For future reference, it is convient to create meaningful symlinks:

```
for l in 1 2; do for i in {01..36..2}; do ln -svn <scandate> $DATA_PATH/GE11-M-${i}L${l}-S/scurve/<symlink>; done; done
for l in 1 2; do for i in {02..36..2}; do ln -svn <scandate> $DATA_PATH/GE11-M-${i}L${l}-L/scurve/<symlink>; done; done
```

In that case, all S-curves sharing the same symbolic name can be analyzed in one command:

```
ana_scans.py scurve --heavy --chamberConfig -c -s <symlink>
```

The sparse output plots can be copied into a single folder with:

```
for l in 1 2; do for i in {01..36}; do cp $DATA_PATH/GE11-M-${i}L${l}*/scurve/<symlink>/SCurveData/Summary.png /nfshome0/gempro/elog/<my_name>/Summary_GE11-M-${i}L${l}.png; done; done
for l in 1 2; do for i in {01..36}; do cp $DATA_PATH/GE11-M-${i}L${l}*/scurve/<symlink>/SCurveData/h2ScurveSigmaDist_All.png /nfshome0/gempro/elog/<my_name>/h2ScurveSigmaDist_All_GE11-M-${i}L${l}.png; done; done
```

Any discrepancy between the results and what expected from the configuration, i.e. the scans or the analysis failing for a part of the detector, should be investigated and tracked in the [GitLab issues](https://gitlab.cern.ch/cmsgemonline/gem-ops/gem-issue-tracker/-/issues).

#### Checking the HV mapping

Checking the HV mapping is one of the last checks to be performed once the front-end communication has been established and the frontend status confirmed. The HV training must be completed as well since signal amplification is required in the chambers.

(To be done one end-cap at a time.)

1. Turn on the HV with the induction gap off
2. Prepare the DAQ setup:
   a. Establish the communication with the front-end
   b. Configure the chambers with a threshold of 180 DAC units
      ```
      for i in {0..11}; do confChamber.py --shelf XX --slot YY -g $i --run --zeroChan --vt1=180 --detType short --gemType=ge11; done
      ```
   c. In `gem_reg.py`, make sure that a few VFAT per chamber readout an S-bit rate of 0 Hz:
      ```
      connect gem-shelfXX-amcYY
      kw GEM_AMC.OH.OHZZ.FPGA.TRIG.CNT.VFAT
      ```
      (By default, this displays the S-bit rate per VFAT in Hertz.)
      **If most of the VFAT in a given chamber do not reach an S-bit rate of 0 Hz, do not proceed further and contact a DAQ expert. Checking the mapping is not going to be successful.**
3. Check the mapping for every chamber individually:
   a. Turn on the induction gap
   b. In `gem_reg.py` make sure that the S-bit rate is different than 0 Hz on the expected chambers (i.e. the two chambers corresponding to the super-chamber):
      ```
      connect gem-shelfXX-amcYY
      kw GEM_AMC.OH.OHZZ.FPGA.TRIG.CNT.VFAT
      ```
      (Tip: the chamber name to DAQ geographical address can be found in the system specific constant file.)
   c. Turn off the induction gap

## Update a CTP7 for the legacy software (DAQ expert only)

When required, the CTP7 firmware can be updated with the following command once the SOCKS proxy is established (`ssh -D 5000 cmsusr` from the `gemvm-legacy` machine):

```
for i in {01..11..2}
do
    ALL_PROXY=socks5h://localhost:5000/ ~/scripts/gemctp7user/setup_ctp7_gempro.sh -c 3.11.4 gem-shelfXX-amc$i
done
mussh -m 12 -H $DATA_PATH/config/ctp7_hosts.txt -l root -c 'chmod -R 777 /mnt/persistent/gemdaq/address_table.mdb/'
```

## Development software

**Warning:** The development software is not compatible with the legacy software and only one of the two versions can run simultaneously.

### Performing updates (DAQ expert only)

#### Client software

The client software is updated with the standard CMS cluster `dropbox2` mechanism. Please refer to the CMS cluster documentation for more information.

#### Backend software

The backend software is currently installed by synchronizing a local folder with the backend boards: 

```
for host in $(cat /gemdata/config/ctp7_hosts.txt)
do
    rsync -v -a --delete --include='/bin/***' --include='/lib/' --include='/lib/gem/***' --exclude='*' <my_local_copy>/mnt/persistent/gempro/ gempro@${host}:
done
```

**TODO:** Simplify the command line. Provide a tool?

#### Firmware

* Install the new firmware files in `/gemdata/firmware/{be_fw,optohybrid_fw}` following the established convention
* Copy the new firmware files in `share/gem/{be_fw,optohybrid_fw}`:
  ```
  for host in $(cat /gemdata/config/ctp7_hosts.txt); do rsync -v -a --delete /gemdata/firmware/{be_fw,optohybrid_fw} gempro@${host}:share/gem/; done
  ```
* Update the firmware related symlinks in `etc/gem/`:
  ```
  mussh -m 12 -H /gemdata/config/ctp7_hosts.txt -l gempro -c 'bash -l -c "ln -sfv ../../share/gem/be_fw/NEW_VERSION_BE.bit etc/gem/gem_ctp7.bit"' 
  mussh -m 12 -H /gemdata/config/ctp7_hosts.txt -l gempro -c 'bash -l -c "ln -sfv ../../share/gem/be_fw/NEW_VERSION_BE.xml etc/gem/gem_ctp7.xml"'
  mussh -m 12 -H /gemdata/config/ctp7_hosts.txt -l gempro -c 'bash -l -c "ln -sfv ../../share/gem/be_fw/NEW_VERSION_BE.cfg etc/gem/gem_ctp7.cfg"'
  mussh -m 12 -H /gemdata/config/ctp7_hosts.txt -l gempro -c 'bash -l -c "ln -sfv ../../share/gem/optohybrid_fw/NEW_VERSION_FE.bit etc/gem/gem_optohybrid.bit"'
  mussh -m 12 -H /gemdata/config/ctp7_hosts.txt -l gempro -c 'bash -l -c "ln -sfv ../../share/gem/optohybrid_fw/NEW_VERSION_FE.xml etc/gem/gem_optohybrid.xml"'
  ```
* Update the LMDB address table:
  ```
  mussh -m 12 -H /gemdata/config/ctp7_hosts.txt -l gempro -c 'bash -l -c "bin/gem-update-address-table"'
  ```

### Recovering the backend boards

The development software, running under the `gempro` user, is recovered with the following procedure:

```
mussh -m 12 -H /gemdata/config/ctp7_hosts.txt -l root -c 'bash -c "killall -9 rpcsvc; killall -9 ipbus"'
mussh -m 12 -H /gemdata/config/ctp7_hosts.txt -l gempro -c 'bash -l -c "killall -9 gemrpc; rm /dev/shm/sem.memhub.gem; bin/gemrpc"'
```
