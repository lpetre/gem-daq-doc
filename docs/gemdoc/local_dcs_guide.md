# Commissioning of the GE2/1 detector HV

The two GE2/1 detectors installed in the 2023-24 YETS are in the negative endcap, sectors 16 and 18, layer 1.
The two detectors are connected to the HV with different schemes:
- the four modules of GE2/1-M-16-L1 are all connected to the same HV channel through a 4-to-1 cable splitter;
- each of the four modules of the GE2/1-M-18-L1 is connected to a separate HV channel and can be controlled independently.

To control and monitor the GE2/1 detector HV before the DCS supports it we are using the [GEM lab DCS](https://gitlab.cern.ch/cms-gem-integration/gem-lab-dcs) package installed in the P5 network.

The HV data are logged on InfluxDB and displayed on the [Grafana GE2/1 HV dashboard](https://cmsgemonline-monitoring.app.cern.ch/d/d216e186-f0b5-4e81-b9f0-5cf3119bf76d/ge2-1-hv?orgId=1&refresh=1m).

## HV control using the "GEM lab DCS"

To use the `gem-lab-dcs` command to control the GE2/1 HV, first log in to the `gemdoc` account on the P5 DAQ machine:
```bash
ssh username@cmsusr
ssh gem-daq01
sudo -u gemdoc -i
```

Read the help message by running `gem-lab-dcs --help` to list the available commands.

Use the command `gem-lab-dcs summary` to get a summary of the detector status. Example:
```bash
[gemdoc@gem-daq01 (srv-s2g18-33-01) ~]$ gem-lab-dcs summary
Mainframe StackMainframe: hostname 10.176.2.210
+-----------------+---------+---------+------------------------+---------+
| Detector        |   Board |   Layer | Divider current (µA)   | Power   |
+=================+=========+=========+========================+=========+
| GE21-M-16-L1    |       0 |       0 | INVALID                | OFF     |
+-----------------+---------+---------+------------------------+---------+
| GE21-M-18-L1-M1 |       0 |       1 | INVALID                | OFF     |
+-----------------+---------+---------+------------------------+---------+
| GE21-M-18-L1-M2 |       1 |       0 | INVALID                | OFF     |
+-----------------+---------+---------+------------------------+---------+
| GE21-M-18-L1-M3 |       1 |       1 | INVALID                | OFF     |
+-----------------+---------+---------+------------------------+---------+
| GE21-M-18-L1-M4 |       2 |       0 | INVALID                | OFF     |
+-----------------+---------+---------+------------------------+---------+
```

To get the status of a single detector, use the command `gem-lab-dcs get DETECTOR-LABEL`. Example:
```bash
[gemdoc@gem-daq01 (srv-s2g18-33-01) ~]$ gem-lab-dcs get GE21-M-18-L1-M4
+-------------+------------+--------------+-------------+----------------+---------+--------------+----------+--------+
| Electrode   |   Vmon (V) |     EDC (µA) |   Imon (µA) |   Imondet (µA) |   Power |   I0Set (µA) |   Status |   Trip |
+=============+============+==============+=============+================+=========+==============+==========+========+
| G3BOT       |       0.12 |    0.192     |      -0.46  |              0 |       0 |           10 |        0 |      0 |
+-------------+------------+--------------+-------------+----------------+---------+--------------+----------+--------+
| G3TOP       |     600.28 | 1143.39      |      -0.263 |              0 |       1 |           10 |        1 |      0 |
+-------------+------------+--------------+-------------+----------------+---------+--------------+----------+--------+
| G2BOT       |       0.1  |    0.114286  |      -0.455 |              0 |       0 |           10 |        0 |      0 |
+-------------+------------+--------------+-------------+----------------+---------+--------------+----------+--------+
| G2TOP       |       0.06 |    0.109091  |      -0.493 |              0 |       0 |           10 |        0 |      0 |
+-------------+------------+--------------+-------------+----------------+---------+--------------+----------+--------+
| G1BOT       |       0    |    0         |      -0.135 |              0 |       0 |           10 |        0 |      0 |
+-------------+------------+--------------+-------------+----------------+---------+--------------+----------+--------+
| G1TOP       |     600.14 | 1071.68      |      -0.143 |              0 |       1 |           10 |        1 |      0 |
+-------------+------------+--------------+-------------+----------------+---------+--------------+----------+--------+
| DRIFT       |       0.06 |    0.0533333 |      -0.282 |              0 |       0 |           10 |        0 |      0 |
+-------------+------------+--------------+-------------+----------------+---------+--------------+----------+--------+
Detector power OFF
Divider status INVALID
Equivalent divider current 255.6 µA
```

To set a single value to a parameter (voltage, current limit or equivalent divider current) to a detector, use the command:
```bash
gem-lab-dcs set DETECTOR-LABEL {vset,i0,divider} VALUE [-e ELECTRODE-NAMES]
```
Examples:
```bash
gem-lab-dcs set GE21-M-18-L1-M1 vset 50 -e foils # sets the VSet of G1TOP, G2TOP, G3TOP to 50 V for GE21-M-18-L1-M1
gem-lab-dcs set GE21-M-18-L1-M1 i0 10 # sets the I0Set of all electrodes to 10 µA for GE21-M-18-L1-M1
gem-lab-dcs set GE21-M-18-L1-M1 divider 700 -e drift g1bot # sets the voltage of DRIFT and G1BOT to an equivalent divider current of 700 µA for GE21-M-18-L1-M1
```

### Shortcuts

For HV training, use the command
```bash
ge21-training VSET-DRIFT VSET-G1TOP VSET-G1BOT VSET-G2TOP VSET-G2BOT VSET-G3TOP VSET-G3BOT
```
to set all voltages at once. The command will increase the I0Set on all electrodes to 20 µA, ramp the electrodes, then wait for 40 seconds for all electrodes to finish ramping and finally set the I0Set back to 10 µA.

## Periodic checks for shifters

The status of the HV has to be checked every 3 hours to ensure the power drained by the detector is not above the expected limit. The checked has to be performed **three times at night** (at 12 AM, 3 AM and 6 AM CERN time).

To check the HV status during your shift, open the [GE2/1 HV dashboard](https://cmsgemonline-monitoring.app.cern.ch/d/d216e186-f0b5-4e81-b9f0-5cf3119bf76d/ge2-1-hv?orgId=1&refresh=1m). For each of the 5 detectors (GE2/1-M-16-L1 + each of the 4 modules of GE2/1-M-18-L1) you will see a list of panels showing:
- the last current and voltage reading on each detector;
- the power status of the detector (off, on or partially on, as expected during HV training);
- the list of channels in trip;
- the voltage trend for each of the electrodes;
- the current trend for each of the electrodes;
- the voltage trend in equivalent-divider current for each of the electrodes.

> **Note:** if you see interrupted trends or "no data" warnings, you might need to increase the time range of the dashboard from the top right and see the history in the last 3 or 6 hours. This might happen because the detectors are very stable and the monitorables are not updated unless they are changing over time.

### What to check during your periodic check

1. If the current reading (IMon) for any electrode is presently above the 10 µA threshold (the current value will be displayed in red), please call the GEM run coordinator (60380) on CERNphone.

    No action is needed if any of the other values (power, channels in trip, voltage or equivalent divider current) are in red or are over any thresholds.

2. ~~Additionally, open the [rack monitoring dashboard](https://cmsgemonline-monitoring.app.cern.ch/d/Qg1GTdfVk/rack-monitoring?orgId=1&from=now-2d&to=now) and check the temperature trend of rack S1G13. If the temperature in the rack has risen over 23 °C in the last three hours, please call the run coordinator.~~ **This check is not needed any more**

**In any case**, please write an elog in the [GEM Commissioning section](https://cmsonline.cern.ch/webcenter/portal/cmsonline/pages_common/elog?__adfpwp_action_portlet=683379043&__adfpwp_backurl=https%3A%2F%2Fcmsonline.cern.ch%3A443%2Fwebcenter%2Fportal%2Fcmsonline%2Fpages_common%2Felog%3F_afrMFO%3D0%26_afrMFR%3D192%26_afrMFS%3D0%26_afrWindowMode%3D0%26_afrMFG%3D0%26_afrMFCI%3D0%26_afrMFH%3D729%26_afrMFM%3D0%26_afrMFDH%3D900%26_afrMFC%3D8%26_afrMT%3Dscreen%26_afrLoop%3D5229048267887324%26_afrFS%3D16%26_afrMFW%3D1440%26Adf-Window-Id%3Dw1d9sml9rcr%26_afrMFDW%3D1440%26__adfpwp_mode.683379043%3D1&_piref683379043.strutsAction=%2FviewSubcatMessages.do%3FcatId%3D1691%26subId%3D1715%26page%3D1) (Home > Subsystems > GEM > P5 > Commissioning) containing **the time you performed the check and the S1G13 temperature**.


### Resources:
- [GE2/1 HV dashboard](https://cmsgemonline-monitoring.app.cern.ch/d/d216e186-f0b5-4e81-b9f0-5cf3119bf76d/ge2-1-hv?orgId=1&refresh=1m)
- [rack monitoring dashboard](https://cmsgemonline-monitoring.app.cern.ch/d/Qg1GTdfVk/rack-monitoring?orgId=1&from=now-2d&to=now)
- run coordinator phone: 60380
