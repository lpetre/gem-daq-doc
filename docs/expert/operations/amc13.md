# AMC13

An overview of the general AMC13 usage is provided in
`userguide:gemos-amc13-guide`{.interpreted-text role="ref"}. More
complete information can be found on the [AMC13 information
page](http://www.amc13.info) maintained by the Boston University group.
Below you will find additional documentaiton and information for more
expert level usage.

## Advanced `AMC13Tool2.exe` usage

An introduction to the [AMC13Tool2.exe]{.title-ref} tool can be found in
`userguide:gemos-amc13-amc13tool`{.interpreted-text role="ref"}. A few
additional commands are described in more detail here.

Some useful commands are:

-   `rg` General reset,
-   `rc` Counter reset,
-   `rd` DAQ Link reset,
-   `pv (pk)` Program the Virtex/Kintex FPGA (T2)
-   `ps` Program the Spartan FPGA (T1)
-   `vv (vk)` Verify the Virtex/Kintex FPGA (T2) against the MCS file
-   `vs` Verify the Spartan FPGA (T1) against the MCS file

### Reloading FW

To reload the FW of the AMC13, from the `AMC13Tool2.exe` CLI execute
`reconfigureFPGAs`.

!!! note
    This will cause the card to be non-responsive for a small amount of
    time. Additionally, it will necessitate a reload of FW of everything
    downlink of the AMC13 (e.g., any CTP7's in the μTCA crate, any
    OptoHybrids connected to those CTP7s, reconfiguring any VFATs connected
    to those OptoHybrids, etc\...). This action should typically not be done
    except in the most dire of circumstances (e.g., when any and all other
    troubleshooting actions have been attempted, and failed). This will then
    require the user to re-enable the clock to all AMC slots of interest in
    a crate following instructions under
    `userguide:gemos-amc13-enabling-clock`{.interpreted-text role="ref"}.

### Updating FW

Have your test stand sysadmin execute the following procedure:

1.  Get the latest file from the [AMC13 FW
    Page](http://ohm.bu.edu/~dgastler/CMS/AMC13-Firmware/?C=M;O=D),

2.  Program the flash of the Virtex (Kintex) FPGA with the `pv` (`pk`)
    command,

3.  Verify the flash of the Virtex (Kintex) FPGA with the `vv` (`vk`)
    command

    !!! important
        If the verification is *not* successful do *not* continue, repeat
        step 2 unitl step 3 succeeds,

4.  Reconfigure the FPGA's following instructions under
    `gemos-amc13-reload-fw`{.interpreted-text role="ref"}.

Note if you execute step 4 without step 3 succeeding you could brick the
board and by extension the μTCA crate. An example of a successful
firmware upgrade can be found in this [elog
entry](http://cmsonline.cern.ch/cms-elog/946282).
