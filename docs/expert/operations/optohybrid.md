# OptoHybrid

The OptoHybrid (OH) serves several purposes in the GEM front-end system.
It concentrates the trigger signals from all VFATs connected to a given
GEB, and links to the back-end via optical links. The optical link data
transmission happens via the GBTx chip with the GBT protocol, and is
managed via a special ASIC for slow control.

## OptoHybrid (OH) FPGA

Unlike the OHv2b, the OHv3 FPGA is not responsible for slow control or
data transfer of tracking data from the VFATs. The OHv3 FPGA deals only
with sending the VFAT trigger data to the CSC OTMB and GEM CTP7.

### Programming the OH FPGA

To program the FPGA we recommend the \"PROM-less\" or `BLASTER(tm)`
method. To program the OH FPGA execute the following:

1.  Power the LV,
2.  Program all GBTs on the OHv3 via one of the methods under
    `gemos-gbt-programming`{.interpreted-text role="ref"},
    -   In the rare case that the GBTx\'s on your OH are fully fused
        proceed to step 3
3.  Issue an sca reset following instructions under
    `gemos-sca-reset`{.interpreted-text role="ref"},
4.  From `gem_reg.py` on the DAQ PC connect to the CTP7 of interest with
    `connect eagleXX` and then enable the TTC generator via
    `write GEM_AMC.TTC.GENERATOR.ENABLE 1`,
5.  From `gem_reg.py` send a single TTC hard reset to program the FPGA
    with the `BLASTER(tm)` via
    `write GEM_AMC.TTC.GENERATOR.SINGLE_HARD_RESET 1`
    -   Note this will issue this reset to *all* optohybrids on this
        CTP7 which *will* stop any existing data taking, crashing any
        scans, and wipe out any present configuration
6.  Check that the FW is loaded into all optohybrids present by
    following instructions at \[Checking Firmware Version\],
7.  From `gem_reg.py` disable the TTC generator via
    `write GEM_AMC.TTC.GENERATOR.ENABLE 0`,
    -   This is important, while the TTC generator is enabled the CTP7
        will *ignore* all TTC commands from the backplane

If you see in step 6 that the FW is not loaded in any of the optohybrids
of interest (or you where expecting a different OH FW version) it is
likely that either the OH FW is *not* loaded into the CTP7 RAM or that a
different version of OH FW is loaded into the CTP7 RAM. To resolve this
login to the CTP7 and execute:

``` bash
cd /mnt/persistent/gemdaq/gemloader
./gemloader_configure.sh
```

Then repeat step 5 again. Note sometimes the OH FW does not load
successfully into the CTP7 RAM and the call of `gemloader_configure.sh`
must be repeated several times. If however after this the FW is not
loading onto one or more optohybrids check to make sure you have
communication with the SCA of interest by following the instructions in
`gemos-sca-status`{.interpreted-text role="ref"}. If you\'re SCA
communication is good and the FW is still not loading double check that
the TTC Generator is enabled by reading the value of the
`GEM_AMC.TTC.GENERATOR.ENABLE`. If the TTC Generator is enabled, the SCA
status is good, and the OH FW is in the CTP7 RAM check to make sure GBT0
is still good, see `gemos-gbt-ready-registers`{.interpreted-text
role="ref"}. If GBT0 is no longer good then programming the FPGA will
not be possible (as this is through GBT0). In this case you may need to
start the procedure again from step 1. One final check would be to
ensure the CTP7 mapping register has the correct value, see
`userguide:gemos-slow-control-ctp7-mapping`{.interpreted-text
role="ref"}.

If after all these you are still *unable* to program the FPGA the linux
image of your CTP7 may be to old, contact your test stand\'s sysadmin.
Although typically this is not the case.

Failure to program the FPGA in our experience usually comes from:

1.  Hardware problem,
2.  Failure to execute the procedure in the correct order

### Checking trigger link status

To check the status of the OH-CTP7 trigger link for OHY execute:

``` bash
kw GEM_AMC.TRIGGER.OHY.LINK
```

Where `Y` is an integer representing the OH number. A healthy link
should come back as:

``` bash
eagleXX > kw GEM_AMC.TRIGGER.OHY.LINK
0x66000e80 r    GEM_AMC.TRIGGER.OHY.LINK0_SBIT_OVERFLOW_CNT             0x00000000
0x66000e80 r    GEM_AMC.TRIGGER.OHY.LINK1_SBIT_OVERFLOW_CNT             0x00000000
0x66000e84 r    GEM_AMC.TRIGGER.OHY.LINK0_MISSED_COMMA_CNT              0x00000000
0x66000e84 r    GEM_AMC.TRIGGER.OHY.LINK1_MISSED_COMMA_CNT              0x00000000
0x66000e8c r    GEM_AMC.TRIGGER.OHY.LINK0_OVERFLOW_CNT                  0x00000000
0x66000e8c r    GEM_AMC.TRIGGER.OHY.LINK1_OVERFLOW_CNT                  0x00000000
0x66000e90 r    GEM_AMC.TRIGGER.OHY.LINK0_UNDERFLOW_CNT                 0x00000000
0x66000e90 r    GEM_AMC.TRIGGER.OHY.LINK1_UNDERFLOW_CNT                 0x00000000
0x66000e94 r    GEM_AMC.TRIGGER.OHY.LINK0_SYNC_WORD_CNT                 0x00000000
0x66000e94 r    GEM_AMC.TRIGGER.OHY.LINK1_SYNC_WORD_CNT                 0x00000000
```

If your link does not look like the above the link is not healthy. First
try reseting the counters and then reading them again by executing:

``` bash
write GEM_AMC.TRIGGER.CTRL.CNT_RESET  1
kw GEM_AMC.TRIGGER.OHY.LINK
```

If your link still does not match the example above try the following:

1.  If the trigger fiber is accessible as a stand alone fiber (e.g., not
    in an MTP12 bundle) check that there is red light in both ends of
    the fiber coming from the OHv3. If so issue a reset, if not
    -   the board may not be on,
    -   the fiber may be faulty,
    -   the VTTx may be faulty, or
    -   the VTTX may not be receiving the correct voltage, check that
        the 2.5V pin on the OH; with no load it should be between
        \[2.45, 2.66\]V.
2.  Try reloading the firmware to the OHv3 by following
    `these instructions<gemos-optohybrid-programming>`{.interpreted-text
    role="ref"}, in rare cases the trigger block of the OH FW does not
    start properly.

### Masking VFATs from trigger

You can write a 24 bit mask to `GEM_AMC.OH.OHX.FPGA.TRIG.CTRL.VFAT_MASK`
to mask a given set of VFATs from the trigger, having a 1 in the
$N^{th}$ bit means the $N^{th}$ VFAT will be masked.

### Temperature monitoring

The FPGA core temperature is accessible from the sysmon registers in the
OHv3 address table, and there are nine additional PT100(0) sensors
located around the board. These PT100(0) sensors are read by the SCA
when monitoring is enabled, see
`gemos-sca-pt100-sensors`{.interpreted-text role="ref"}. The SCA gives
output in ADC counts. For the details of how the conversions for
temperature and voltages are done see
`gemos-sca-conversion`{.interpreted-text role="ref"}
