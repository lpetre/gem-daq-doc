# GBTx

The
[GBTx](http://iopscience.iop.org/article/10.1088/1748-0221/10/03/C03034/meta)
is a radiation hard gigabit transceiver for optical links which provides
simultaneous transfer of readout data, timing and trigger signals, as
well as slow control and monitoring information.

## Programming GBTx

### Via Dongle: `gbtProgrammer`

To program a GBTx using the USB dongle programmer ensure that the `I2C`
jumper is enabled (e.g., closed circuit) then:

1.  launch the programmer software by executing `gbtProgrammer` from
    terminal,
2.  an error window stating `No WindowsLookAndFeel` will pop up, this is
    normal, press \"okay\",
3.  if an error message loads after the main GUI loads stating:
    `No GBTX detected!` then execute the substeps here, if not proceed
    to step 4,
    -   Close the programmer software,
    -   Disconnect the USB cable from the dongle (this ensures the
        dongle powers off),
    -   Reconnect the USB cable to the dongle (the light should blink),
        then
    -   Repeat steps 1-3
4.  Press `Import i...` to import the configuration file, if a warning
    window pops up asking you to upgrade the dongle SW press `No`,
5.  In the open dialog box that loads change the `Files of Type`
    selection from `XML` to `txt`
6.  If you do not see the GBT configuration file you\'re looking for
    navigate to (on GEM 904 machines only):
    -   `/data/bigdisk/GEMDAQ_Documentation/system/OptoHybrid/V3/GBT_Files/`
7.  Select the configuration file of interest note that files are named
    `GBTX_OHv3Y_GBT_Z_*.txt` where `Y = {a,b,c}` for optohybrid version
    and `Z = {0,1,2}` for GBTx index
    -   Each GBTx will have a different configuration, additionally
        optohybrids on long & short detectors will also have a different
        configuration so you must select the right file for the
        corresponding hardware
8.  Press `Write GBTX`, finally
9.  Press `Read GBTX` if the readback state is anything but `idle 18h`
    then programming failed.

In some cases the readback state will read `idle 18h` but communication
with the GBTx will not be good. To check this:

1.  Navigate to the `Monitoring` tab,
2.  Press the `Monitor!` button, wait some time, the red and blue lines
    can take any values, but they must be *flat* and unchanging in time,
    if not, there's a problem, then
3.  Stop monitoring by pressing `Monitor!` button again.

Note that while monitoring is running the USB cable will induce a large
amount of noise into the front-end electronics. This will be detected by
scurves having a much larger width. If monitoring is running, your noise
will be higher and this can disturb data-taking.

#### Manually writing charge pump current

Ask the sysadmin of your test stand if it is necessary to change the
charge pump current value of the GBT after programming with the USB
dongle, if so while having `gbtProgrammer` open execute:

1.  Navigate to the `Advanced mode` tab,
2.  In the bottom right corner, insert `35` as `Register #` and the
    press `READ`.
3.  The correct value should be `F2` and usually it's already in the
    register.
4.  If it's not, insert `F2` the click Write (hex) value, press `WRITE`
    and repeat step 2.

### Over fiber: `gbt.py`

To program the GBTx over the fiber link it must be at least minimally
fused (so that it locks to the fiber link) and the `I2C` jumper for the
GBTx in question must *not* be in place (e.g., open circuit). Before
proceeding please check that the GBTx communication is good by following
instructions to check the GBTx status on a given ON under Section
`gemos-gbt-ready-registers`{.interpreted-text role="ref"}. Once
communication is enabled exectue the following procedure:

1.  login to the CTP7 of choice as `gemuser` (e.g.,
    `ssh gemuser@eagle60`),

2.  Once logged in, to configure GBT `X` of OH `Y` execute: ..
    code-block:: bash

    > gbt.py Y X config \<config file>

The GBTx will now be programmed. The GBT config files a CTP7 can be
found under:

``` bash
/mnt/persistent/gemdaq/gbt
```

### Performing a GBT phase scan

Again, the GBTx must be at least minimally fused (so that it locks to
the fiber link) and the `I2C` jumper for the GBTx in question must *not*
be in place (e.g., open circuit). Before proceeding please check that
the GBTx communication is good by following instructions to check the
GBTx status on a given ON under Section
`gemos-gbt-ready-registers`{.interpreted-text role="ref"}. Once
communication is enabled exectue the following procedure:

``` bash
gbt.py Y X v3b-phase-scan <config file> 2>&1 | tee $HOME/oh_Y_gbt_X_phase_scan.txt
```

This will scan all phases for all e-links on this GBTx and report
whether the phase is good (bad) if the `SYNC_ERR_CNT` of the VFAT on
that e-link is `0x1` (`0x0`). Note that while the above says
`v3b-phase-scan` it is good for any v3 optohybrid version. The GBT
config files a CTP7 can be found under:

``` bash
/mnt/persistent/gemdaq/gbt
```

### Fusing

This can only be done with the USB dongle and this should be done only
by true hardware experts with consent of GEM DAQ team (if you are
wondering if you fall in this category it probably means you should not
be fusing GBTs) as this process is irreversible and if done incorrectly
could brick communication with one or more VFATs, the FPGA, or the
entire front-end electronics. To fuse a GBTx launch the `gbtProgrammer`
software and then execute the following procedure:

1.  `Import` config file,
2.  `Write GBT`,
3.  `Read GBT`,
4.  State should be `idle 18h`, if not stop and investigate,
5.  Go to `Monitoring` tab
6.  Press `Monitor!`, wait some time, the red and blue lines can take
    any values, but they must be "flat" and unchanging in time, if not,
    there's a problem
7.  Stop monitoring (press `Monitor!` button a second time)
8.  Leave the `gbtProgrammer` window running
9.  In a separate terminal in `gem_reg.py` verify that the `GBT_READY`
    register of this GBTx is `0x1`,
10. Issue a link reset then read the errors flags for this GBTx (NOT
    ready, was not ready) to ensure they are `0x0`,
11. Check that `SYNC_ERR_CNT` of all VFATs on this GBTx are `0x0` and
    the counters do not roll up,
    -   See `userguide:gemos-gbt-elinks`{.interpreted-text role="ref"}
        for GBTx-VFAT correspondence
12. Check that you have slow control with all VFATs on this GBTx by
    executing `kw CFG_RUN <OH Number>` only `0x0` should be returned for
    the VFATs on this GBTx, if not there is a problem,
    -   In rare cases `SYNC_ERR_CNT` are all `0x0` but VFAT
        communication is dead),
13. Go back to the `gbtProgrammer` window,
14. Go to the `Fuse My GBT` tab,
15. Click `Update view`,
16. Make sure all rows in the table are green,
    -   The last row is a test register and it's okay if it's red,
17. Click `Select non zero val…`
18. Check the `enable…` box inside the `Fuse GBTX` box, this enables
    fusing,
19. Check the `fuse upda…` check box inside the `Fuse GBTX` box, this
    fuses the GBTx such that after power on reset it loads it's fuse
    settings (not doing this means fusing was useless :D),
20. Click `Fuse` button in the `Fuse GBTX` box,
21. Close the `gbtProgrammer` software,
22. Power off the OHv3,
23. Leaving the USB dongle connected to the GBTx of your OHv3,
    disconnect the usb cable from the dongle,
    -   Failure to do this will leave the GBTx partially powered from
        the USB cable and result in a \"funky" unusable state,
24. Power on the OHv3,
25. Plug the USB cable back into the dongle,
26. Launch `gbtProgrammer`,
27. Import the config file that you used for fusing,
28. Click `Read GBTx` and check state is `idle 18h`,
29. Go to the `Fuse My GBTx` tab,
30. Click `Update View`, then
31. Check if all rows in table are green (it could be that the last row
    is red...it's okay if it's only this one).

The GBTx is now fused.

### `writeGBTPhase.py`: Manually writing the GBT e-link phase for a given VFAT

You can write the GBT e-link phase for a given VFAT or all VFATs using
the `writeGBTPhase.py` tool by calling from the DAQ machine:

``` bash
writeGBTPhase.py -h
```

which should produce output:

``` bash
usage: writeGBTPhase.py [-h] {single,all} ...

Tool for writing GBT phase for a single or all elink

positional arguments:
  {single,all}  Available subcommands and their descriptions.To view the sub
                menu call writeGBTPhase.py COMMAND -h e.g.
                writeGBTPhase.py single -h
    single      write GBT phase for single VFAT
    all         write GBT phase for all VFAT

optional arguments:
  -h, --help    show this help message and exit
```

There are two sub-commands `single` and `all`. To check their details:

``` bash
writeGBTPhase.py single -h
```

which should produce output:

``` bash
usage: writeGBTPhase.py single [-h] shelf slot vfat phase link

positional arguments:
  shelf       μTCA shelf number
  slot        AMC slot number in the μTCA shelf
  vfat        VFAT number on the OH
  phase       GBT Phase Value to Write
  link        OH number on the AMC

optional arguments:
  -h, --help  show this help message and exit
```

and

``` bash
writeGBTPhase.py all -h
```

which should produce output:

``` bash
usage: writeGBTPhase.py all [-h] shelf slot gbtPhaseFile

positional arguments:
  shelf         μTCA shelf number
  slot          AMC slot number in the μTCA shelf
  gbtPhaseFile  File having link, vfat and phase info.
                The input file will look like:
                --------------------------
                link/i:vfatN/i:GBTPhase/i:
                4    0    7
                4    1    9
                4    2    13
                --------------------------

optional arguments:
  -h, --help    show this help message and exit
```

For example:

1.  If you want to write phase for single VFAT:

    ``` bash
    > writeGBTPhase.py single 1 6 23 7 3
    ```

    this will write the phase 7 to VFAT23 on
    `(shelf,slot,link) = (1,6,3)`.

2.  If you want to write phase for all VFAT using input text file as
    expected by the script:

    ``` bash
    > writeGBTPhase.py all 1 6
    > $DATA_PATH/GE11-X-S-INDIA-0015/gbtPhaseSetPoints_GE11-X-S-INDIA-0015_current.log
    ```

    Here, we assumed that we are reading the detector
    `GE11-X-S-INDIA-0015`. This will write phases for all VFATs on
    `(shelf, slot) = (1, 6)` for the link told by the text file.
