# P5 operations

P5 operations are the primary support provided by the GEM DAQ expert
team.

## Adding GEM user accounts

Adding a new GEM user to the P5 machines is handled by the CMS sysadmin
team. The GEM responsible should open a new [Jira
ticket](https://its.cern.ch/jira/projects/CMSONS) reqesting the NICE
username be added to the technical network, as well as the `gempro`
group.

### Adding an account to a GEM managed machine

In the case of machines which are not administered by the sysadmins, you
can add NICE users to the machines to grant other CERN users access to
the GEM machines. This can be done via the
:cpp`setupMachine.sh script<gemctp7user:setup_machine>`{.interpreted-text
role="func"} script, with the `-u` option. You should have a text file
with one entry per line (the NICE username).

!!! note
    The script will loop through all the names, so you can use it to add
    multiple users in one go, but it is interactive, as it also allows you
    to add the users to various privilegd groups and this has not yet been
    scripted.
