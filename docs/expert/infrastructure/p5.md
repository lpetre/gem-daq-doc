# Infrastructure at p5

The GEM infrastructure at p5 is partly managed by the CMS sysadmin team, and partly managed by the GEM DAQ group.
The sysadmins are responsible for computer hardware and ensuring software is able to be installed in an automatic (Puppetized) way.
They are also responsible for the network configuration.

The GEM DAQ experts are responsible for the GEM DAQ harware and updating software repository profiles in the software dropbox.

## Sysadmin support

Support from the sysadmin team can be sought several ways.
The on-call CMS DAQ expert can initiate support for issues related to the central DAQ infrastructure (e.g. miniDAQ).
For all other issues, the CMS sysadmins utilize a Jira ticketing system ([described here](https://twiki.cern.ch/twiki/bin/view/CMS/Net-and-Sys-admin-Users)).
To open a ticket you must be a member of the appropriate e-group and then use the [CMSONS Jira portal](https://its.cern.ch/jira/browse/CMSONS).

## Computers

Machines controlled by GEM:

| Hostname                | Machine alias   | OS            | Notes                                    |
|-------------------------|-----------------|---------------|------------------------------------------|
| `srv-s2g18-33-01`       | `gem-daq01`     | AlmaLinux 9.2 | Data taking applications, PowerEdge R340 |
| `srv-s2g18-31-01`       | `gem-daq02`     | AlmaLinux 9.2 | DAQ services, PowerEdge R340             |
| `srv-s2g18-34-01`       | `gem-locdaq01`  | AlmaLinux 9.2 | Local readout, PowerEdge R440            |
| `srv-s2g18-32-01`       | `gem-locdaq02`  | AlmaLinux 9.2 | Analysis suite, PowerEdge R440           |
| `kvm-s3562-1-ip156-65`  | `gemvm-control` | AlmaLinux 9.2 | GEM DAQ head node, VM                    |
| `kvm-s3562-1-ip157-30`  | `gemvm-test`    | AlmaLinux 9.2 | Test/development machine[^1], VM         |

[^1]: Typically for unmanaged services (e.g. the GE2/1 detector safety system)
    or development purposes. While the machine is managed via Puppet, tools and
    libraries are manually installed whenever needed (`gcc`, `java`, `git`,...)
    Do not rely on system persistence! The GEM Dropbox is also disabled on that
    specific machine, so that no GEM RPM packages are available and
    automatically installed.

Machines used by GEM:

| Hostname                | Machine alias               | OS         | Notes                                             |
|-------------------------|-----------------------------|------------|---------------------------------------------------|
| `ctrl-s2c17-22-01`      | `ctrlhub-gem-01`[^2]        | CentOS 7.6 | `sysmgr` and `control_hub` host                   |
| `kvm-s3562-1-ip156-16`  | `cmsrc-gem`, `gemrc-gemdev` | CentOS 7.9 | `RCMS` host (i.e., function managers)             |
| `fu-c2f11-23-03`        |                             | CentOS 7.9 | DAQ2 MiniDAQ DQM                                  |
| `dqmfu-c2b03-46-01`     |                             | RedHat 8.7 | DAQ3 MiniDAQ DQM (shared between Muon subsystems) |

[^2]: Additional µTCA-based aliases: `bridge-s2e01-04`, `bridge-s2e01-14`,
    `bridge-s2e01-23`

### Remote powering

Instructions on remotely powering on/off the centrally managed GEM machines can be found on the P5 cluster users guide twiki [IPMI instructions](https://twiki.cern.ch/twiki/bin/view/CMS/ClusterUsersGuide#IPMI).

!!! warning
    These on/off actions should **only** be taken after confirmation that it is OK from the CMS Technical Coordination and CMS sysadmins.

### Software dropbox

Information on using the `dropbox2` tool to update the software associated with
the puppetized machines can be found on the P5 cluster users guide twiki
[dropbox
link](https://twiki.cern.ch/twiki/bin/view/CMS/ClusterUsersGuide#How_to_use_the_dropbox_computer).

!!! note
    For machines installed with CentOS 7.x, the `cmsdropbox` machine must be
    used while for machines installed with AlmaLinux 9.x, the `cmsdropboxel9`
    machine must be used. In both cases, the `dropbox2` script must be used.

!!! note
    If a package has not previously been included in a GEM puppet profile, a
    [Jira ticket](https://its.cern.ch/jira/projects/CMSONS) should be
    opened, providing the initial RPM. Following this, updating the RPM is
    done in the normal way with the `dropbox2` tool.

### Monitoring (icinga)

Monitoring of central infrastructure is provided via `icinga`: <http://cmsicinga2.cms/icingaweb2>
SMS notifications are sent to a list of CERN mobile numbers, the DAQ expert phone is one.
Email notifications are sent to the members of the `cms-gem-online-notifications` e-group via the `cms-gem-daq-notifications` email address.
The notification polices can be changed by opening a [Jira ticket](https://its.cern.ch/jira/projects/CMSONS), using any configuration defined in [this documentation](https://docs.icinga.com/latest/en/notifications.html).

## DAQ hardware

### Racks

#### S2E01

| Device            | Notes                                              |
|-------------------|----------------------------------------------------|
| `psu-s2e01-24-01` | GE1/1 Aspiro PSU                                   |
| `mch-s2e01-23-01` | GE-1/1 µTCA crate - FED1467 (GEM-)                 |
| `mch-s2e01-14-01` | GE+1/1 µTCA crate - FED1468 (GEM+)                 |
| `psu-s2e01-05-01` | GE2/1 Aspiro PSU                                   |
| `mch-s2e01-04-01` | GE2/1 µTCA crate - FED1469 (GEMPILOT) |

#### S2E02

| Device               | Notes                         |
|----------------------|-------------------------------|
| `sw-eth-s2e02-15-01` | Local readout Ethernet switch |

### Fibers

The complete DAQ fiber mapping can be found in [this document](fiber-mapping.txt).
