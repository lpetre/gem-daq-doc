# Data taking

The data taking on the stack requires two steps:

1. Configuring the front-end and starting a run (from the GEM DAQ web interface);
2. Enabling the local readout, i.e. the data saving on the DAQ machine through ethernet.

## Starting a run

1. Open the xDAQ interface on the DAQ machine at [http://pcuclacvp13:20500](http://pcuclacvp13:20500);
2. Click on "gem supervisor GEMSupervisor";
3. If a run is ongoing (the state is "Running"), click on "Stop";
4. Reconfigure the front-end by clicking on "Configure";
5. Ensure that the OptoHybrid mask in the AMCManager message corresponds to the present layout (if necessary, refresh the web page to ensure the OptoHybrid mask appears). For example, for a stack of three layers you expect the message:

> Configured OptoHybrid mask 0x7 (initial mask 0x7)

6. Click on "Start".

After the run has started, have a look at the data taking monitoring:

1. Go back to the main xDAQ webpage;
2. Click on "gem monitor FEDMonitor";
3. If the monitoring is not running (i.e. all the cells are empty), move to the "Expert Page" tab and start it by clicking on "Configure AMCs" and then on "Start AMCs Monitoring";
4. In the TTC section, confirm that the "L1A_ID" field is increasing over time and that the "L1A_RATE" field shows a rate of a few Hz (this may depend on your trigger configuration).

## Starting the local readout

The local readout application is a terminal application running on the stack DAQ machine.

1. Log in through SSH to the DAQ machine;
2. Attach to the local DAQ session with the command
```bash
tmux attach -t local-daq
```
3. Start the local readout with the command
```bash
start-run
```
4. Following the command line prompt, enter your name, run purpose (e.g. "HV scan point at 600 µA") and run type (e.g. physics);
5. A few lines containing the local readout configuration will be printed out, followed by lines similar to the following:
```
Available buffers: 3071 / 4095

Port:  0  Tx: 0 Rx: 0 Miss: 0 Tot: 0 Perc: -nan%
Port:  1  Tx: 0 Rx: 0 Miss: 0 Tot: 0 Perc: -nan%
          ---------------------------------------
Tot:      Tx: 0 Rx: 0 Miss: 0 Tot: 0 Perc: -nan%

Queue:  0 Rx: 0 Enq: 0 EnqFail: 0 Perc: -nan%

-------------------------------------------------
Nothing to do on port 1 (0 queues), terminating lcore

-------------------------------------------------

Available buffers: 3076 / 4095

Port:  0  Tx: 1 Rx: 5 Miss: 0 Tot: 5 Perc: 0.000%
Port:  1  Tx: 0 Rx: 0 Miss: 0 Tot: 0 Perc: -nan%
          ---------------------------------------
Tot:      Tx: 1 Rx: 5 Miss: 0 Tot: 5 Perc: 0.000%

Queue:  0 Rx: 5 Enq: 5 EnqFail: 0 Perc: 0.000%
```
The printout indicates that the data saving is ongoing. Monitor for a few seconds the `Tot` field in port 0 to ensure that the counter increases.
6. Detach from the local readout session by pressing `Ctrl+b`, then the key `d`.
!!! warning "Pieter warning"

    Please, do not press `Ctrl+c` or `Ctrl+d` to close your session, as this will either stop the local readout or kill the local readout session.
7. Close your SSH session.
8. If you have anything specific other people might need to know about this run, please log a summary of your activity (e.g. why are you taking this run, what are the conditions of the setup) in an [elog](https://cmsonline.cern.ch/webcenter/portal/cmsonline/pages_common/elog) (subcategory Home > Subsystems > GEM > 904 Integration).

## Stopping a run

1. To stop the local readout, reattach to the local readout tmux session (steps 1 and 2 from the previous session), then press `Ctrl+c` and wait a few seconds;
2. Press `Enter` when a prompt appears;
3. Copy the last few lines and paste them at the end of your elog, if any. This will contain your messages, the date and times of the beginning and end of the runs and the total number of events.

## What to do after stopping a run

Check that the run is entered correctly in the [shared run spreadsheet](https://docs.google.com/spreadsheets/d/1Jsv9CvGpQpqyGNdgWzRpfyUqBE-mlXyN1yWsdtutqIo/edit?usp=sharing).
Apart from the lines automatically added at the end of the run, you might need to add:

- the HV of the detector (if not all detectors had the same HV point, write "mixed"; we can figure out the details from the elog);
- the source status (for example if you are at GIF++) and the particle type;
- if the data taking went wrong, type "no" in the "GOOD run?" field, otherwise type yes;
- unless you copied the run data to the shared `eos` folder, type "no" in the "DATA in EOS?" field;
- paste a link to your elog in the "elog" field;
- if you have run results, e.g. plots, paste a link to them in the "Results" field.